       IDENTIFICATION DIVISION. 
       PROGRAM-ID. EDIT-1.
       AUTHOR. JARUPHONG.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NumericVallue  PIC 9(8)V99 VALUE 00014584.95.
       01  Edit1          PIC 99,999,999.99.
       01  Edit2          PIC ZZ,ZZZ,ZZ9.99.
       01  Edit3          PIC $*,***,**9.99.
       01  Edit4          PIC ++,+++,++9.99.
       01  Edit5          PIC $$,$$$,$$9.99.
       01  Edit6          PIC $$,$$$,$$9.00.
       01  Edit7          PIC 99/999/999.99.
       01  Edit8          PIC 99999000999.99.
       01  Edit9          PIC 99999BBB999.99.
       PROCEDURE DIVISION.
       BEGIN.
           MOVE NumericVallue TO Edit1
           DISPLAY "Edit1 = " Edit1

           MOVE NumericVallue TO Edit2
           DISPLAY "Edit2 = " Edit2

           MOVE NumericVallue TO Edit3
           DISPLAY "Edit3 = " Edit3

           MOVE NumericVallue TO Edit4
           DISPLAY "Edit4 = " Edit4
      
           MOVE NumericVallue TO Edit5
           DISPLAY "Edit5 = " Edit5
      
           MOVE NumericVallue TO Edit6
           DISPLAY "Edit6 = " Edit6
      
           MOVE NumericVallue TO Edit7
           DISPLAY "Edit7 = " Edit7
           .
